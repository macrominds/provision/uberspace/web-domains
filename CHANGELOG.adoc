= Change Log

All noteable changes to this project will be documented in this file.
This project adheres to http://semver.org/[Semantic Versioning].

== [1.0.0] – 2022-10-29

* fix: outdated pipeline by using a proven setup
* fix: outdated molecule lint syntax

== [untagged] – 2019-12-06

* basic functionality
